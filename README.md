### Desafio Control
Olá, queremos convidá-lo a participar de nosso desafio de seleção. Pronto para participar? Seu trabalho será visto por nosso time e você receberá ao final um feedback sobre o que achamos do seu trabalho. Não é legal?

### Sobre a Oportunidade
A vaga é para Analista de Sistemas, se você for aprovado nesta etapa, será convidado para uma entrevista com nosso time de especialistas. Orientações:

* Você pode utilizar alguma das seguintes linguagens (Ruby on Rails, PHP, Python ou Java);
* Você pode utilizar algum dos seguintes bancos de dados (SQLite, PostgreSQL, MySQL)

### Pré-Requisitos
Criar um sistema para cadastro de ordem de serviço que possua:

* Criação, edição, exclusão e listagem de serviços;
* Criação, edição, exclusão e listagem de ordens de serviço;
* Relatório para contabilização dos serviços realizados e respectivos valores.

#### Campo para Serviço
| Campo   | Tipo    | Obrigatório | Observação |
| --------|---------|-------|-------|
| descricao | Texto   |  Sim | Descrição do serviço |
| valor | Monetário   |  Sim | Valor cobrado pela unidade do serviço |


#### Campo para Ordem de Serviço
Obs.: Uma ordem de serviço possui apenas um único serviço

| Campo           | Tipo     | Obrigatório | Observação |
| -------- |--------- |------- |------- |
| serviço         | Serviço  | Sim         | Serviço executado na orde de serviço abela que agrupe |
| quantidade      | Numérico | Sim         | Quantidade do serviço executado. Mínimo 1 |
| nomeFuncionario | Texto    | Sim         | Nome do funcionário que executou a ordem de serviço |
| data            | Data     | Sim         | Data da execução da ordem de serviço |
| horaInicio      | Hora     | Sim         | Hora de início da execução da ordem de serviço |
| horaFim         | Hora     | Sim         | Hora de término da execução da ordem de serviço |
| detalhe         | Texto    | Não         | Detalhes adicionais sobre a ordem de serviço |


#### Relatório de Contabilização dos Serviços
Página que deve exibir uma tabela que agrupe todos os serviços executados nas ordens de serviço e as respectivas quantidades e valores. 
A contabilização dos serviços possuem as regras:

1. A cada 10 unidades de um serviço, esse serviço recebe 10% de desconto.
2. O desconto máximo que um serviço pode receber é de 30%

Exemplo tabela:

| Serviço | Valor Unitário | Qtd | Valor Total | Desconto | Valor Final|
| -------- |--------- |------- |------- |------- |------- |
| Corte de energia     | R$45,00 | 15 |   R$675,00 | 10% |   R$607,50 |
| Inspeção             | R$20,00 |  8 |   R$160,00 |  0% |   R$160,00 |
| Religação de energia | R$30,00 | 50 | R$1,500,00 | 30% | R$1.050,00 |
| TOTAL                |         |    | R$2.335,00 |     | R$1.817,50 |


### Escopo Mínimo

* Listagem de serviços cadastrados
* Criação e edição de serviços
* Exclusão de serviços
* Listagem das ordens de serviços cadastradas
* Criação e edição de ordem de serviço
* Exclusão de ordem de serviço
* Exibição do relatório de contabilização dos serviços
* Elaborar um manual de execução da aplicação


### Diferenciais

* Utilização de algum framework de layout (bootstrap, materialize, etc)
* Implementar autenticação


### Instruções

1. Faça o fork do desafio;
2. Crie um repositório privado no bitbucket para o projeto e adicione como colaborador o usuário sistemas_control;
3. Desenvolva. Você terá até 7 (sete) dias a partir da data do envio do desafio; 
4. Após concluir seu trabalho faça um push; 
5. Responda ao e-mail enviado do desafio, adicionando cópia para sistemas@control.eng.br notificando a finalização do desafio para validação.



## *Tutorial de Instalação*


### *Tecnologias utilizadas e seus links para download*
* Hibernate
* AngularJS
* Bootstrap
* [NPM](https://www.npmjs.com/package/npm)
* Bower
* [Java 1.8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
* MySQL
* Spring Boot
* [Maven](https://maven.apache.org/download.cgi)
* Servidor MySQL local rodando (Easy PHP DEVSERVER, XAMPP ou similar)

### *Construindo o projeto*
1. Tenha certeza que o NPM, Java 1.8, Maven e o servidor MySQL estão instalando e funcionando apropriadamente em sua máquina local
2. Crie um banco de dados MySQL no seu servidor local e sete no arquivo ```application.properties``` na pasta ```desafio_control\src\main\resources``` a url do caminho do banco de dados, seu usuário e senha de acesso ao banco
   - Exemplo: ```spring.datasource.url=jdbc:mysql://localhost/controldb```
```spring.datasource.username=root```
```spring.datasource.password=```
3. Tenha certeza que seu servidor MySQL (podendo ser no Easy PHP ou qualquer outra plataforma que crie um servidor MySQL local) está rodando localmente e com banco de dados criado
4. Faça o clone do projeto para a sua máquina local: ```git clone https://feitosajorge@bitbucket.org/feitosajorge/desafio_control.git```
5. Navegue até a pasta raiz do projeto ```desafio_control```
6. Tenha certeza que tem o NPM instalado em sua máquina e está na variável de ambiente do sistema
7. Instale o Bower usando o comando NPM via linha de comando: ```npm install -g bower```
8. Execute via linha de comando ainda na pasta raiz do projeto: ```bower install```
9. Tenha certeza que contém o Maven instalado em sua máquina ou em sua IDE, caso queira usar um para rodar o código
10. Execute o comando na pasta raiz do projeto via CMD: ```mvn clean install -DskipTests``` o -DskipTests é para pular os testes enquanto instalando, não é obrigatório usar
   - Pode executar o maven usando sua IDE, ao invés

### *Como executar o projeto*
* Execute o comando abaixo na raiz do projeto ```desafio_control```:
```mvn spring-boot:run```
* Acesse sua porta local com o /control/ como base, exemplo:
```http://127.0.0.1:8080/control/```

### *Screenshots*

![página inicial](https://i.imgur.com/7s7d4fp.png)

![página de ordens de serviço](https://i.imgur.com/KrfbdLy.png)

![página de relatório](https://i.imgur.com/cokRbIc.png)