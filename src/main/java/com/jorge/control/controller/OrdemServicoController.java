package com.jorge.control.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jorge.control.domain.OrdemServico;
import com.jorge.control.dto.OrdemServicoDTO;
import com.jorge.control.service.OrdemServicoService;

/**
 * Classe responsável por configurar todas as execuções via REST do Sprint Boot
 */
@RestController
@RequestMapping("/ordemServico")
public class OrdemServicoController {
	
	// Invoca o service da ordem de serviço para ser efetuado execuções geralmente para o banco de dados
	@Autowired
    private OrdemServicoService ordemServicoService;
	
	/**
	 * Adiciona um novo serviço baseado nas informações passadas
	 * @param ordemServicoDTO dados do serviço passado nos parâmetros
	 * @return os dados salvos do serviço ao banco de dados
	 */
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public OrdemServico adicionarItemDaOrdem(@RequestBody OrdemServicoDTO ordemServicoDTO) {
        return ordemServicoService.salvarOrdem(ordemServicoDTO);
    }

    
    /**
     * Retorna todos os itens das ordens de serviços presentes no banco de dados
     * @return todos os itens das ordens de serviços em um array
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<OrdemServico> getAll(){
        return ordemServicoService.findAll();
    }
    
    /**
     * Edita os dados da ordem de serviço específico e salva no banco 
     * @param ordemServicoDTO a ordem de serviço a ser editado
     * @param ids ID da ordem serviço a ser encontrado e editado
     * @return os dados editados da ordem serviço
     */
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json", consumes = "application/json", value ="/{id}")
    public OrdemServico atualizarItemDaOrdem(@RequestBody OrdemServicoDTO ordemServicoDTO, @PathVariable("id") Long ids) {
        return ordemServicoService.atualizarOrdem(ordemServicoDTO, ids);
    }
    
    /**
     * Item da ordem serviço a ser deletado
     * @param ids número do ID da ordem de serviço a ser deletado
     */
    @RequestMapping(method = RequestMethod.DELETE, value ="/{id}")
    public void deletarItemDaOrdem(@PathVariable("id") Long ids) {
    	ordemServicoService.deletarOrdem(ids);
    }
    
    /**
     * Limpa todos os dados da ordem serviços presentes no banco de dados
     * @param object 
     */
    @RequestMapping(method = RequestMethod.DELETE)
    public void limparOrdens( Object object) {
    	ordemServicoService.limparOrdens(object);
    }

}
