package com.jorge.control.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jorge.control.domain.OrdemServico;
import com.jorge.control.dto.RelatorioDTO;
import com.jorge.control.service.OrdemServicoService;

/**
 * Classe responsável por configurar todas as execuções via REST do Sprint Boot
 */
@RestController
@RequestMapping("/relatorio")
public class RelatorioController {
	
	// Invoca o service da ordem de serviço para ser efetuado execuções geralmente para o banco de dados
	@Autowired
    private OrdemServicoService ordemServicoService;
	
	/**
	 * Captura todos os itens das ordens de serviço, filtra e retorna os dados apresentáveis para a página de relatório
	 * @return todos os dados necessários para o relatório
	 */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public Map<String, Object> getAll(){
    	
    	Map<String, Object> relatorioCompleto = new HashMap<>();
    	
    	List<RelatorioDTO> relatorioLista = new ArrayList<>();
    	
    	// Encontra todos os items das ordens de serviço
        List<OrdemServico> ordemServicoLista = ordemServicoService.findAll();
        
        for(OrdemServico ordemServico : ordemServicoLista) {
        	
        	// Se o ID do serviço já foi encontrada
        	boolean achou = false;
        	
        	for(int i = 0; i < relatorioLista.size(); i++) {
        		
        		if(ordemServico.getServico().getId() == relatorioLista.get(i).getIdServico()) {
        			//Se foi encontrado o ID do serviço já presente no array do relatório então some, multiplique valores como quantidade das ordens
        			RelatorioDTO relatorio = new RelatorioDTO();
        			relatorio.setIdServico(ordemServico.getServico().getId());
            		relatorio.setDescricaoServico(ordemServico.getServico().getDescricao());
            		relatorio.setValorUnitarioServico(ordemServico.getServico().getValor());
            		relatorio.setQuantidadeOrdem(relatorioLista.get(i).getQuantidadeOrdem() + ordemServico.getQuantidade());
            		relatorio.setValorTotalOrdem(relatorio.getValorUnitarioServico() * relatorio.getQuantidadeOrdem());
            		relatorio.setDescontoOrdem(calcularDesconto(relatorio.getQuantidadeOrdem() + ordemServico.getQuantidade()));
            		relatorio.setValorFinalOrdem(relatorio.getValorTotalOrdem() - (relatorio.getValorTotalOrdem() * relatorio.getDescontoOrdem()) / 100);
            		relatorioLista.set(i, relatorio);
        			achou = true;
        			break;
        		}
        	}
        	
        	if(!achou) {
        		// Um novo ID de serviço foi encontrada
        		RelatorioDTO relatorio = new RelatorioDTO();
        		relatorio.setIdServico(ordemServico.getServico().getId());
        		relatorio.setDescricaoServico(ordemServico.getServico().getDescricao());
        		relatorio.setValorUnitarioServico(ordemServico.getServico().getValor());
        		relatorio.setQuantidadeOrdem(ordemServico.getQuantidade());
        		relatorio.setValorTotalOrdem(ordemServico.getServico().getValor() * relatorio.getQuantidadeOrdem());
        		relatorio.setDescontoOrdem(calcularDesconto(relatorio.getQuantidadeOrdem()));
        		relatorio.setValorFinalOrdem(relatorio.getValorTotalOrdem() - (relatorio.getValorTotalOrdem() * relatorio.getDescontoOrdem()) / 100);
        		relatorioLista.add(relatorio);
        	}
        }
        
        Double valorTotal = 0.0;
        Double valorFinal = 0.0;
        
        for(RelatorioDTO relatorio : relatorioLista) {
        	//Soma o valor total e o valor final de todos os itens do relatório presentes no array
        	valorTotal = valorTotal + relatorio.getValorTotalOrdem();
        	valorFinal = valorFinal + relatorio.getValorFinalOrdem();
        }
        
        relatorioCompleto.put("listaRelatorio", relatorioLista); //Salva a lista filtrada das ordens de serviço
        relatorioCompleto.put("valorTotal", valorTotal); //Retorna o valor total de todos as ordens de serviço
        relatorioCompleto.put("valorFinal", valorFinal); //Retorna o valor final de todos as ordens de serviço com desconto
        
        return relatorioCompleto;
    }
    
    /**
     * Calcula o desconto baseado na quantidade de ordens, se a quantidade for menor que 10, retorna 0 de desconto, por exemplo.
     * @param quantidade de ordens encontrada
     * @return o desconto total
     */
    private Double calcularDesconto(Integer quantidade) {
    	Double desconto = 0.0;
    	
    	if(quantidade < 10) {
    		return 0.0;
    	} else if (quantidade >=10 && quantidade < 20) {
    		return 10.0;
    	} else if (quantidade >=20 && quantidade < 30) {
    		return 20.0;
    	} else if (quantidade >=30) {
    		return 30.0;
    	}
    	
    	return desconto;
    }

}
