package com.jorge.control.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jorge.control.domain.Servico;
import com.jorge.control.dto.ServicoDTO;
import com.jorge.control.service.ServicoService;

import java.util.List;

/**
 * Classe responsável por configurar todas as execuções via REST do Sprint Boot
 */
@RestController
@RequestMapping("/servico")
public class ServicoController {
	
	// Invoca o service do serviço para ser efetuado execuções geralmente para o banco de dados
	@Autowired
    private ServicoService servicoService;
	
	/**
	 * Adiciona um novo serviço baseado nas informações passadas
	 * @param servicoDTO dados do serviço passado nos parâmetros
	 * @return os dados salvos do serviço ao banco de dados
	 */
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public Servico adicionarItemDoServico(@RequestBody ServicoDTO servicoDTO) {
        return servicoService.salvarServico(servicoDTO);
    }

    
    /**
     * Retorna todos os itens de serviços presentes no banco de dados
     * @return todos os itens do serviço em um array
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Servico> getAll(){
        return servicoService.findAll();
    }
    
    /**
     * Edita os dados de um serviço específico e salva no banco 
     * @param servico o serviço a ser editado
     * @param ids ID do serviço a ser encontrado e editado
     * @return os dados editados do serviço
     */
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json", consumes = "application/json", value ="/{id}")
    public Servico atualizarItemDoServico(@RequestBody Servico servico, @PathVariable("id") Long ids) {
        return servicoService.atualizarServico(servico, ids);
    }
    
    /**
     * Item do serviço a ser deletado
     * @param ids número do ID do serviço a ser deletado
     */
    @RequestMapping(method = RequestMethod.DELETE, value ="/{id}")
    public void deletarItemDoServico(@PathVariable("id") Long ids) {
    	servicoService.deletarServico(ids);
    }
    
    /**
     * Limpa todos os dados de serviço presentes no banco de dados
     * @param object 
     */
    @RequestMapping(method = RequestMethod.DELETE)
    public void limparServicos( Object object) {
    	servicoService.limparServicos(object);
    }

}
