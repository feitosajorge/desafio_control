package com.jorge.control.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jorge.control.ControlServiceApp;
import com.jorge.control.domain.OrdemServico;
import com.jorge.control.domain.Servico;
import com.jorge.control.dto.OrdemServicoDTO;
import com.jorge.control.repository.OrdemServicoRepository;
import com.jorge.control.repository.ServicoRepository;

/**
 * Classe que oferece serviços de receber e filtrar dados da requisição via REST como deletar, adicionar ou atualizar cada campo
 */
@Service
@Transactional
public class OrdemServicoService {
	
	// Invoca o repositório das ordens de serviço e seus respectivos comandos JPA
	@Autowired
    private OrdemServicoRepository ordemServicoRepository;
	
	// Invoca o repositório do serviço e seus respectivos comandos JPA
	@Autowired
    private ServicoRepository servicoRepository;
	
    private static List<OrdemServico> ordemServico = new ArrayList<>();
    
    static {
    }
    
    /**
     * Adiciona informações iniciais ao banco, caso o mesmo esteja vazio
     */
    public void saveInitialBatch() {
    	ordemServicoRepository.save(ordemServico);
        ordemServico.forEach((ordemServicos) -> {
            ControlServiceApp.jmsTemplate.convertAndSend("salvarordem", ordemServicos);
        });
    }
    
    /**
     * Encontra e retorna todos os dados de ordem de serviço na tabela de ordem de serviço no banco de dados
     * @return lista de ordens de serviço encontrados
     */
    public List<OrdemServico> findAll() {
        return ordemServicoRepository.findAll();
    }
    
    /**
     * Salva os dados da ordem de serviço requisitados, ao banco de dados
     * @param OrdemServicoDTO dados vindo da view para ser persistido
     * @return os dados da ordem de serviço salvo
     */
    public OrdemServico salvarOrdem(OrdemServicoDTO ordemServicoDTO) {
    	OrdemServico ordemServico = new OrdemServico();
        Servico servico = servicoRepository.findOne(ordemServicoDTO.getServicoId());
        ordemServico.setServico(servico);
        ordemServico.setQuantidade(ordemServicoDTO.getQuantidade());
        ordemServico.setNomeFuncionario(ordemServicoDTO.getNomeFuncionario());
        ordemServico.setData(ordemServicoDTO.getData());
        ordemServico.setHoraInicio(ordemServicoDTO.getHoraInicio());
        ordemServico.setHoraFim(ordemServicoDTO.getHoraFim());
        ordemServico.setDetalhe(ordemServicoDTO.getDetalhe());
        ControlServiceApp.jmsTemplate.convertAndSend("salvarordem", ordemServico);
        return ordemServicoRepository.save(ordemServico);
    }
    
    /**
	 * Atualiza os dados de uma única ordem de serviço, pesquisando pelo o ID do mesmo
	 * @param ordemServicoDTO a ordem de serviço a ser editado os dados
	 * @param id o ID da ordem de serviço a ser pesquisado
	 * @return os dados editados salvos no banco da ordem de serviço
	 */
    public OrdemServico atualizarOrdem(OrdemServicoDTO ordemServicoDTO, Long id) {
    	OrdemServico atualizarOrdem = ordemServicoRepository.findOne(id);
    	Servico servico = servicoRepository.findOne(ordemServicoDTO.getServicoId());
    	atualizarOrdem.setServico(servico);
    	atualizarOrdem.setQuantidade(ordemServicoDTO.getQuantidade());
    	atualizarOrdem.setNomeFuncionario(ordemServicoDTO.getNomeFuncionario());
		atualizarOrdem.setData(ordemServicoDTO.getData());
		atualizarOrdem.setHoraInicio(ordemServicoDTO.getHoraInicio());
		atualizarOrdem.setHoraFim(ordemServicoDTO.getHoraFim());
    	atualizarOrdem.setDetalhe(ordemServicoDTO.getDetalhe());
    	ControlServiceApp.jmsTemplate.convertAndSend("atualizarordem", atualizarOrdem);
        return ordemServicoRepository.save(atualizarOrdem);
    }
    
    /**
     * Deleta a ordem de serviço específica, baseado no ID
     * @param id o ID da ordem de serviço a ser pesquisado e deletado
     */
    public void deletarOrdem(Long id) {
    	ControlServiceApp.jmsTemplate.convertAndSend("deletarordem", id);
    	ordemServicoRepository.delete(id);
    }
    
    /**
     * Apaga todos os dados da ordem de serviço da tabela
     * @param object
     */
    public void limparOrdens(Object object) {
    	ControlServiceApp.jmsTemplate.convertAndSend("limparordens", new Date());
        ordemServicoRepository.delete(findAll());
    }

}
