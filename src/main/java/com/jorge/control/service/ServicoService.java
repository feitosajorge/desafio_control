package com.jorge.control.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jorge.control.ControlServiceApp;
import com.jorge.control.domain.OrdemServico;
import com.jorge.control.domain.Servico;
import com.jorge.control.dto.ServicoDTO;
import com.jorge.control.repository.OrdemServicoRepository;
import com.jorge.control.repository.ServicoRepository;

/**
 * Classe que oferece serviços de receber e filtrar dados da requisição via REST como deletar, adicionar ou atualizar cada campo
 */
@Service
@Transactional
public class ServicoService {
	
	// Invoca o repositório do serviço e seus respectivos comandos JPA
	@Autowired
    private ServicoRepository servicoRepository;
	
	// Invoca o repositório das ordens de serviço e seus respectivos comandos JPA
	@Autowired
    private OrdemServicoRepository ordemServicoRepository;

    private static List<Servico> servico = new ArrayList<>();
    
    /**
     * Adiciona informações iniciais ao banco, caso o mesmo esteja vazio
     */
    static {
    	servico.add(new Servico("Corte de energia", 45.00));
    	servico.add(new Servico("Inspeção", 20.00));
    	servico.add(new Servico("Religação de energia", 30.00));
    }
    
    /**
     * Adiciona informações iniciais ao banco, caso o mesmo esteja vazio
     */
    public void saveInitialBatch() {
    	if(servicoRepository.findAll() == null || servicoRepository.findAll().isEmpty()) {
	    	servicoRepository.save(servico);
	    	servico.forEach((servicos) -> {
	            ControlServiceApp.jmsTemplate.convertAndSend("salvarservico", servicos);
	        });
    	}
    }
    
    /**
     * Encontra e retorna todos os dados de serviço na tabela de serviço no banco de dados
     * @return lista de serviços encontrados
     */
    public List<Servico> findAll() {
        return servicoRepository.findAll();
    }
    
    /**
     * Salva os dados do serviço requisitados, ao banco de dados
     * @param servicoDTO dados vindo da view para ser persistido
     * @return os dados do serviço salvo
     */
	public Servico salvarServico(ServicoDTO servicoDTO) {
		Servico servico = new Servico();
		servico.setDescricao(servicoDTO.getDescricao());
		servico.setValor(servicoDTO.getValor());
        ControlServiceApp.jmsTemplate.convertAndSend("salvarservico", servico);
        return servicoRepository.save(servico);
    }

	
	/**
	 * Atualiza os dados de um único serviço, pesquisando pelo o ID do mesmo
	 * @param servico o serviço a ser editado os dados
	 * @param id o ID do serviço a ser pesquisado
	 * @return os dados editados salvos no banco do serviço
	 */
    public Servico atualizarServico(Servico servico, Long id) {
    	Servico atualizarItem = servicoRepository.findOne(id);
    	atualizarItem.setValor(servico.getValor());
    	atualizarItem.setDescricao(servico.getDescricao());
    	ControlServiceApp.jmsTemplate.convertAndSend("atualizarservico", atualizarItem);
        return servicoRepository.save(atualizarItem);
    }
    
    /**
     * Deleta um serviço baseado no ID do mesmo e seus respectivos filhos na tabela da ordem de serviço.
     * 
     * Será removido primeiro os dados da tabela da ordem de serviço que contenha o ID do serviço e logo após será removido 
     * o próprio serviço
     * @param id ID do serviço a ser removido 
     */
    public void deletarServico(Long id) {
    	List<OrdemServico> orderServicoLista = ordemServicoRepository.findByServico(id);
    	for(OrdemServico ordemServico : orderServicoLista) {
    		ordemServicoRepository.delete(ordemServico.getId());
    		ControlServiceApp.jmsTemplate.convertAndSend("deletarordem", ordemServico.getId());
    	}
    	
    	ControlServiceApp.jmsTemplate.convertAndSend("deletarservico", id);
    	servicoRepository.delete(id);
    }
    
    /**
     * Apaga todos os dados de serviço da tabela
     * @param object
     */
    public void limparServicos(Object object) {
    	ControlServiceApp.jmsTemplate.convertAndSend("limparservicos", new Date());
    	servicoRepository.delete(findAll());
    }

}
