package com.jorge.control.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jorge.control.domain.Funcionario;

/**
 * Respositório dos funcionários contendo funções básicas para executar no banco de dados como CRUD
 */
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {
    
}
