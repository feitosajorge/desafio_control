package com.jorge.control.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jorge.control.domain.OrdemServico;

/**
 * Respositório das ordens de serviço contendo funções básicas para executar no
 * banco de dados como CRUD
 */
public interface OrdemServicoRepository extends JpaRepository<OrdemServico, Long> {

	/**
	 * Encontra todas as ordens de serviço usando o ID do serviço como referência
	 * @param id ID do serviço a ser encontrada
	 * @return a lista de todas as ordens de serviço encontradas baseados no ID do serviço
	 */
	@Query(value = "SELECT * FROM ordem_servico os WHERE os.servico = :servicoId", nativeQuery = true)
	public List<OrdemServico> findByServico(@Param("servicoId") Long id);
}
