package com.jorge.control.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jorge.control.domain.Servico;

/**
 * Respositório dos serviços contendo funções básicas para executar no banco de dados como CRUD
 */
public interface ServicoRepository extends JpaRepository<Servico, Long> {

}
