package com.jorge.control.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Classe JPA responsável por criar a tabela no banco de dados e suas respectivas colunas de acordo com as variáveis setadas nesta classe.
 */
@Entity
@Table(name = "servico")
public class Servico implements Serializable {
	
	// Cria o ID do serviço que terá como nome do campo id no formato bigint(20) e será auto incremental
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	// Cria o campo da descrição no formato varchar com largura 255 e não pode ser nulo
	@Column(name = "descricao")
	@NotNull
	private String descricao;
	
	// Cria o campo do valor no formato double e não pode ser nulo
	@Column(name = "valor")
	@NotNull
	private Double valor;
	
	/**
	 * Construtor básico
	 */
	public Servico() {

	}
	
	/**
	 * Construtor que passa a descrição e o valor como parâmetro
	 * @param descricao a ser setada na variável principal
	 * @param valor a ser setado na variável principal
	 */
	public Servico(String descricao, Double valor) {
		this.setDescricao(descricao);
		this.setValor(valor);
	}

	/**
	 * @return o ID a ser retornado
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 * 		o ID a ser inserido
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return a descrição a ser retornada
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * @param descricao
	 * 		a descrição a ser inserida
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * @return o valor a ser retornado
	 */
	public Double getValor() {
		return valor;
	}
	
	/**
	 * @param valor
	 * 			o valor a ser inserido
	 */
	public void setValor(Double valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return String.format("Servico{descricao=%s, valor=%s}", getDescricao(), getValor());
	}

}
