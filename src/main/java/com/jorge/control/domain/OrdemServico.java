package com.jorge.control.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * Classe JPA responsável por criar a tabela no banco de dados e suas respectivas colunas de acordo com as variáveis setadas nesta classe.
 */
@Entity
@Table(name = "ordem_servico")
public class OrdemServico implements Serializable {
	
	// Cria o ID do da orddem de serviço que terá como nome do campo id no formato bigint(20) e será auto incremental
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	// Cria o campo do servico no formato bigint(20) e não pode ser nulo
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "servico")
	@NotNull
	private Servico servico;
	
	// Cria o campo da quantidade no formato int(11) e não pode ser nulo
	@Column(name = "quantidade")
	@NotNull
	private Integer quantidade;
	
	// Cria o campo do nome do funcionário no formato varchar com largura 255 e não pode ser nulo
	@Column(name = "nome_funcionario")
	@NotNull
	private String nomeFuncionario;
	
	// Cria o campo da data no formato varchar com largura 255 e não pode ser nulo
	@Column(name = "data")
	@NotNull
	private String data;
	
	// Cria o campo da hora início no formato varchar com largura 255 e não pode ser nulo
	@Column(name = "hora_inicio")
	@NotNull
	private String horaInicio;
	
	// Cria o campo da hora fim no formato varchar com largura 255 e não pode ser nulo
	@Column(name = "hora_fim")
	@NotNull
	private String horaFim;
	
	// Cria o campo da descrição no formato varchar com largura 512 e PODE ser nulo
	@Column(name = "detalhe", length=512)
	private String detalhe;
	
	//Construtor básico
	public OrdemServico() {
	}
	
	/**
	 * Construtor onde será passado todos os valores, exceto o ID da ordem de serviço como parâmetro
	 * @param servico O serviço relacionado a ordem
	 * @param quantidade A quantidade da ordem do serviço
	 * @param nomeFuncionario O nome do funcionário que executou a ordem
	 * @param data A data que iniciou o serviço
	 * @param horaInicio A hora que iniciou o serviço
	 * @param horaFim A hora que encerrou o serviço
	 * @param detalhe O detalhe sobre o serviço executado
	 */
	public OrdemServico(Servico servico, Integer quantidade, String nomeFuncionario, String data, String horaInicio,
			String horaFim, String detalhe) {
		this.servico = servico;
		this.quantidade = quantidade;
		this.nomeFuncionario = nomeFuncionario;
		this.data = data;
		this.horaInicio = horaInicio;
		this.horaFim = horaFim;
		this.detalhe = detalhe;
	}

	/**
	 * @return o ID a ser retornado
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 * 			o ID a ser inserido
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return o servico a ser retornado
	 */
	public Servico getServico() {
		return servico;
	}

	/**
	 * @param servico
	 *            o servico a ser inserido
	 */
	public void setServico(Servico servico) {
		this.servico = servico;
	}

	/**
	 * @return a quantidade a ser retornada
	 */
	public Integer getQuantidade() {
		return quantidade;
	}

	/**
	 * @param quantidade
	 *            a quantidade a ser inserida
	 */
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	/**
	 * @return o nomeFuncionario a ser retornado
	 */
	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	/**
	 * @param nomeFuncionario
	 *            o nomeFuncionario a ser inserido
	 */
	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	/**
	 * @return a data a ser retornada
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data
	 *            a data a ser inserida
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return a horaInicio a ser retornada
	 */
	public String getHoraInicio() {
		return horaInicio;
	}

	/**
	 * @param horaInicio
	 *            a horaInicio a ser inserida
	 */
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	/**
	 * @return a horaFim a ser retornada
	 */
	public String getHoraFim() {
		return horaFim;
	}

	/**
	 * @param horaFim
	 *            a horaFim a ser inserida
	 */
	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}

	/**
	 * @return o detalhe a ser retornado
	 */
	public String getDetalhe() {
		return detalhe;
	}

	/**
	 * @param detalhe
	 *            o detalhe a ser inserido
	 */
	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}

	@Override
	public String toString() {
		return String.format("OrdemServico{servico=%s, quantidade=%s, nomeFuncionario=%s, detalhe=%s}",
				getServico().getDescricao(), getQuantidade(), getNomeFuncionario(), getDetalhe());
	}

}
