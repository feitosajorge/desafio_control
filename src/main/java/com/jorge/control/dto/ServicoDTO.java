package com.jorge.control.dto;

/**
 * DTO (Data Transfer Object) usado para transferir dados entre a camada de visão e de persistência (model)
 */
public class ServicoDTO {
	
	// Descrição do serviço
	private String descricao;
	
	// Valor do serviço
	private Double valor;

	/**
	 * @return a descricao a ser retornada
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao
	 *            a descricao a ser inserida
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return o valor a ser retornado
	 */
	public Double getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            o valor a ser inserido
	 */
	public void setValor(Double valor) {
		this.valor = valor;
	}

}
