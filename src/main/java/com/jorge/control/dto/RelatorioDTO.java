package com.jorge.control.dto;

/**
 * DTO (Data Transfer Object) usado para transferir dados entre a camada de visão e de persistência (model)
 */
public class RelatorioDTO {
	
	// ID do Serviço
	private Long idServico;
	
	// Descrição do Serviço
	private String descricaoServico;
	
	// Valor padrão do serviço
	private Double valorUnitarioServico;
	
	// Quantidade da ordem de serviço
	private Integer quantidadeOrdem;
	
	// Valor total da ordem de serviço
	private Double valorTotalOrdem;
	
	// Desconto adicionado no valor total da ordem de serviço
	private Double descontoOrdem;
	
	// Valor final da ordem de serviço com desconto
	private Double valorFinalOrdem;

	/**
	 * @return a descricaoServico a ser retornada
	 */
	public String getDescricaoServico() {
		return descricaoServico;
	}

	/**
	 * @param descricaoServico
	 *            a descricaoServico a ser inserida
	 */
	public void setDescricaoServico(String descricaoServico) {
		this.descricaoServico = descricaoServico;
	}

	/**
	 * @return o valorUnitarioServico a ser retornado
	 */
	public Double getValorUnitarioServico() {
		return valorUnitarioServico;
	}

	/**
	 * @param valorUnitarioServico
	 *            o valorUnitarioServico a ser inserido
	 */
	public void setValorUnitarioServico(Double valorUnitarioServico) {
		this.valorUnitarioServico = valorUnitarioServico;
	}

	/**
	 * @return a quantidadeOrdem a ser retornada
	 */
	public Integer getQuantidadeOrdem() {
		return quantidadeOrdem;
	}

	/**
	 * @param quantidadeOrdem
	 *            a quantidadeOrdem a ser inserida
	 */
	public void setQuantidadeOrdem(Integer quantidadeOrdem) {
		this.quantidadeOrdem = quantidadeOrdem;
	}

	/**
	 * @return o valorTotalOrdem a ser retornado
	 */
	public Double getValorTotalOrdem() {
		return valorTotalOrdem;
	}

	/**
	 * @param valorTotalOrdem
	 *            o valorTotalOrdem a ser inserido
	 */
	public void setValorTotalOrdem(Double valorTotalOrdem) {
		this.valorTotalOrdem = valorTotalOrdem;
	}

	/**
	 * @return o descontoOrdem a ser retornado
	 */
	public Double getDescontoOrdem() {
		return descontoOrdem;
	}

	/**
	 * @param descontoOrdem
	 *            o descontoOrdem a ser inserido
	 */
	public void setDescontoOrdem(Double descontoOrdem) {
		this.descontoOrdem = descontoOrdem;
	}

	/**
	 * @return o valorFinalOrdem a ser retornado
	 */
	public Double getValorFinalOrdem() {
		return valorFinalOrdem;
	}

	/**
	 * @param valorFinalOrdem
	 *            o valorFinalOrdem a ser inserido
	 */
	public void setValorFinalOrdem(Double valorFinalOrdem) {
		this.valorFinalOrdem = valorFinalOrdem;
	}

	/**
	 * @return o idServico a ser retornado
	 */
	public Long getIdServico() {
		return idServico;
	}

	/**
	 * @param idServico
	 *            o idServico a ser inserido
	 */
	public void setIdServico(Long idServico) {
		this.idServico = idServico;
	}

}
