package com.jorge.control.dto;

import java.util.regex.PatternSyntaxException;

/**
 * DTO (Data Transfer Object) usado para transferir dados entre a camada de visão e de persistência (model)
 */
public class OrdemServicoDTO {
	
	// O ID do serviço relacionado a ordem
	private Long servicoId;
	
	// A quantidade da ordem do serviço
	private Integer quantidade;
	
	// O nome do funcionário que executou a ordem
	private String nomeFuncionario;
	
	// A data que iniciou o serviço
	private String data;
	
	// A hora que iniciou o serviço
	private String horaInicio;
	
	// A hora que encerrou o serviço
	private String horaFim;
	
	// O detalhe sobre o serviço executado
	private String detalhe;

	/**
	 * @return o servicoId a ser retornado
	 */
	public Long getServicoId() {
		return servicoId;
	}

	/**
	 * @param servicoId
	 *            o servicoId a ser inserida
	 */
	public void setServicoId(Long servicoId) {
		this.servicoId = servicoId;
	}

	/**
	 * @return a quantidade a ser retornado
	 */
	public Integer getQuantidade() {
		return quantidade;
	}

	/**
	 * @param quantidade
	 *            a quantidade a ser inserida
	 */
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	/**
	 * @return a horaInicio a ser retornado
	 */
	public String getHoraInicio() {
		return horaInicio;
	}

	/**
	 * Verifica se a hora início é válida, caso não seja, retorna <code>PatternSyntaxException</code>
	 * @param horaInicio
	 *            a horaInicio a ser inserida
	 */
	public void setHoraInicio(String horaInicio) {
		
		if(horaInicio != null && !horaInicio.matches("([01]?[0-9]|2[0-3]):[0-5][0-9]")) {
    		throw new PatternSyntaxException("O formato da hora de início é inválida. Exemplo de hora válida: 13:16", "([01]?[0-9]|2[0-3]):[0-5][0-9]", -1);
    	}
		
		this.horaInicio = horaInicio;
	}

	/**
	 * @return a horaFim a ser retornado
	 */
	public String getHoraFim() {
		return horaFim;
	}

	/**
	 * Verifica se a hora fim é válida, caso não seja, retorna <code>PatternSyntaxException</code>
	 * @param horaFim
	 *            a horaFim a ser inserida
	 */
	public void setHoraFim(String horaFim) {
		
		if(horaFim != null && !horaFim.matches("([01]?[0-9]|2[0-3]):[0-5][0-9]")) {
    		throw new PatternSyntaxException("O formato da hora de encerramento é inválida. Exemplo de hora válida: 13:16", "([01]?[0-9]|2[0-3]):[0-5][0-9]", -1);
    	}
		
		this.horaFim = horaFim;
	}

	/**
	 * @return o detalhe a ser retornado
	 */
	public String getDetalhe() {
		return detalhe;
	}

	/**
	 * @param detalhe
	 *            o detalhe a ser inserida
	 */
	public void setDetalhe(String detalhe) {
		if(detalhe != null && detalhe.length() > 508) {
			detalhe = detalhe.substring(0, 500).concat("...");
		}
		
		this.detalhe = detalhe;
	}

	/**
	 * @return o nomeFuncionario a ser retornado
	 */
	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	/**
	 * @param nomeFuncionario
	 *            o nomeFuncionario a ser inserida
	 */
	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	/**
	 * @return a data a ser retornado
	 */
	public String getData() {
		return data;
	}

	/**
	 * Verifica se a data é válida, caso não seja, retorna <code>PatternSyntaxException</code>
	 * @param data a data a ser inserida
	 */
	public void setData(String data) {
		
		if(data != null && !data.matches("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/\\d{4}")) {
    		throw new PatternSyntaxException("O formato da data é inválida. Exemplo de data válida: 26/11/2018", "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/\\d{4}", -1);
    	}
		
		this.data = data;
	}

}
