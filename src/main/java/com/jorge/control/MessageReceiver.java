package com.jorge.control;

import java.util.Date;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.jorge.control.domain.Funcionario;
import com.jorge.control.domain.OrdemServico;
import com.jorge.control.domain.Servico;

/**
 * Classe responsável por configurar o envio e recebimento de mensagens através
 * de toda a aplicação, quando informação capturada será apresentada no LOG do
 * seu terminal
 */
@Component
public class MessageReceiver {

	@JmsListener(destination = "savefuncionarioparabd", containerFactory = "myFactory")
	public void receiveFuncionarioSavedMessage(Funcionario funcionario) {
		System.out.println("Funcionario salvo <" + funcionario + ">");
	}

	@JmsListener(destination = "salvarordem", containerFactory = "myFactory")
	public void receiveOrdemSavedMessage(OrdemServico ordemServico) {
		System.out.println("Ordem do Serviço salvo <" + ordemServico + ">");
	}

	@JmsListener(destination = "atualizarordem", containerFactory = "myFactory")
	public void receiveOrdemUpdatedMessage(OrdemServico ordemServico) {
		System.out.println("Ordem do Serviço atualizado <" + ordemServico + ">");
	}

	@JmsListener(destination = "deletarordem", containerFactory = "myFactory")
	public void receiveOrdemDeletedMessage(Long id) {
		System.out.println("Ordem do Serviço deletao <" + id + ">");
	}

	@JmsListener(destination = "limparordens", containerFactory = "myFactory")
	public void receiveOrdemClearedMessage(Date date) {
		System.out.println("List de Ordem de Serviços limpo <" + date + ">");
	}

	@JmsListener(destination = "salvarservico", containerFactory = "myFactory")
	public void receiveServiceSavedMessage(Servico servico) {
		System.out.println("Serviço salvo <" + servico + ">");
	}

	@JmsListener(destination = "atualizarservico", containerFactory = "myFactory")
	public void receiveServiceUpdatedMessage(Servico servico) {
		System.out.println("Serviço atualizado <" + servico + ">");
	}

	@JmsListener(destination = "deletarservico", containerFactory = "myFactory")
	public void receiveServiceDeletedMessage(Long id) {
		System.out.println("Serviço deletado <" + id + ">");
	}

	@JmsListener(destination = "limparservicos", containerFactory = "myFactory")
	public void receiveServiceClearedMessage(Date date) {
		System.out.println("List de serviços limpo <" + date + ">");
	}

}
