package com.jorge.control;

import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import com.jorge.control.service.OrdemServicoService;
import com.jorge.control.service.ServicoService;
import com.jorge.control.service.FuncionarioService;

@SpringBootApplication
@EnableJms
public class ControlServiceApp {
    
	//Template do Message Receiver
    public static JmsTemplate jmsTemplate;

    @Bean
    public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer) {
    	// Configura o Listener do Message Receiver
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    @Bean 
    public MessageConverter jacksonJmsMessageConverter() {
    	// Configura o Message Receiver para o envio de sinais para o LOG quando necessário
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }
    
    @PostConstruct
    public void init(){
        TimeZone.setDefault(TimeZone.getTimeZone("America/Sao_Paulo"));   // Setar a timezone
    }

    public static void main(String[] args) {
    	// Inicia a aplicação
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ControlServiceApp.class, args);
        
        // Configura o bean do message receiver
        jmsTemplate = applicationContext.getBean(JmsTemplate.class);
        
        // Carrega informações iniciais para as ordens de serviço no banco de dados
        OrdemServicoService ordemServicoServiceService = applicationContext.getBean(OrdemServicoService.class);
        ordemServicoServiceService.saveInitialBatch();
        
        // Carrega informações iniciais para os serviço no banco de dados
        ServicoService servicoService = applicationContext.getBean(ServicoService.class);
        servicoService.saveInitialBatch();
        
        // Carrega informações iniciais para os funcionarios no banco de dados
        FuncionarioService funcionarioService = applicationContext.getBean(FuncionarioService.class);
        funcionarioService.saveInitialBatch();

    }
}
