package com.jorge.control;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.jorge.control.ControlServiceApp;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ControlServiceApp.class)
@WebAppConfiguration
public class ControlServiceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
